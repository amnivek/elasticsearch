{
    "unique": 2,
    "from": 0,
    "size": 8,
    "query": {
        "function_score": {
            "query": {
                "multi_match": {
                    "query": "新浪",
                    "type": "most_fields",
                    "fields": ["exact^64", "content^20", "title^30", "description^10", "url^5", "host^25"]
                }
            },
            "functions": [{
                "field_value_factor": {
                    "field": "boost",
                    "missing": 0.5
                }
            }, {
                "gauss": {
                    "pubdate": {
                        "scale": "10000w",
                        "offset": "1w",
                        "decay": 0.05
                    }
                }
            }],
            "boost_mode": "multiply"
        }
    },
    "highlight": {
        "fields": {
            "content": {
                "fragment_size": 70,
                "no_match_size": 70,
                "number_of_fragments": 1,
                "boundary_chars": ".,!?; \t\n，。！？；"
            },
            "description": {
                "fragment_size": 70,
                "no_match_size": 70,
                "number_of_fragments": 1,
                "boundary_chars": ".,!?; \t\n，。！？；"
            }
        }
    },
    "_source": ["title", "url", "protocol", "lang", "pubdate", "isarticle", "isforum", "ismobile", "isquestion", "isroot", "sitelinks", "props", "scopes", "image"]
}