{
    "unique": 2,
    "from": 0,
    "size": 8,
    "query": {
        "filtered": {
            "query": {
                "multi_match": {
                    "query": "新浪",
                    "type": "most_fields",
                    "fields": ["exact^64", "content^20", "title^30", "description^10", "url^5", "host^25"]
                }
            },
            "filter": {
                "bool": {
                    "should": [{
                        "term": {
                            "isarticle": true
                        }
                    }, {
                        "term": {
                            "lang": "zh"
                        }
                    }, {
                        "range": {
                            "pubdate": {
                                "gte": "now-3M"
                            }
                        }
                    }]
                }
            }
        }
    },
    "highlight": {
        "fields": {
            "content": {
                "fragment_size": 70,
                "no_match_size": 70,
                "number_of_fragments": 1,
                "boundary_chars": ".,!?; \t\n，。！？；"
            },
            "description": {
                "fragment_size": 70,
                "no_match_size": 70,
                "number_of_fragments": 1,
                "boundary_chars": ".,!?; \t\n，。！？；"
            }
        }
    },
    "_source": ["title", "url", "protocol", "lang", "pubdate", "isarticle", "isforum", "ismobile", "isquestion", "isroot", "sitelinks", "props", "scopes", "image"]
}