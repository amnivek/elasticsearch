{
    "settings": {
        "number_of_shards": 8,
        "number_of_replicas": 1,
        "index.refresh_interval": "300s"
    },
    "mappings": {
        "_default_": {
            "_all": {
                "enabled": false
            }
        },
        "cn": {
            "_source": {
                "excludes": [
                    "exact"
                ]
            },
            "properties": {
                "boost": {
                    "type": "float",
                    "index": "no",
                    "store": true,
                    "fielddata": {
                        "format": "array"
                    }
                },
                "site": {
                    "type": "string",
                    "index": "not_analyzed",
                    "term_vector": "no",
                    "norms": {
                        "enabled": false
                    },
                    "fielddata": {
                        "format": "disabled"
                    }
                },
                "group": {
                    "type": "binary",
                    "index": "not_analyzed",
                    "store": true,
                    "fielddata": {
                        "format": "doc_values"
                    }
                },
                "host": {
                    "type": "string",
                    "index": "analyzed",
                    "term_vector": "no",
                    "norms": {
                        "enabled": false
                    },
                    "store": false,
                    "fielddata": {
                        "format": "disabled"
                    }
                },
                "protocol": {
                    "type": "string",
                    "index": "no",
                    "store": false,
                    "fielddata": {
                        "format": "disabled"
                    }
                },
                "url": {
                    "type": "string",
                    "index": "analyzed",
                    "term_vector": "no",
                    "norms": {
                        "enabled": false
                    },
                    "store": false,
                    "fielddata": {
                        "format": "disabled"
                    }
                },
                "isroot": {
                    "type": "boolean",
                    "index": "not_analyzed",
                    "store": false,
                    "fielddata": {
                        "format": "disabled"
                    }
                },
                "exact": {
                    "type": "string",
                    "index": "analyzed",
                    "store": false,
                    "fielddata": {
                        "format": "disabled"
                    },
                    "analyzer": "string_synonym",
                    "search_analyzer": "keyword"
                },
                "title": {
                    "type": "string",
                    "index": "analyzed",
                    "store": false,
                    "fielddata": {
                        "format": "disabled"
                    },
                    "term_vector": "with_positions_offsets",
                    "analyzer": "ik_index",
                    "search_analyzer": "ik_search"
                },
                "content": {
                    "type": "string",
                    "index": "analyzed",
                    "store": false,
                    "fielddata": {
                        "format": "disabled"
                    },
                    "term_vector": "with_positions_offsets",
                    "analyzer": "ik_index",
                    "search_analyzer": "ik_search"
                },
                "description": {
                    "type": "string",
                    "index": "analyzed",
                    "store": false,
                    "fielddata": {
                        "format": "disabled"
                    },
                    "term_vector": "with_positions_offsets",
                    "analyzer": "ik_index",
                    "search_analyzer": "ik_search"
                },
                "lang": {
                    "type": "string",
                    "index": "not_analyzed",
                    "norms": {
                        "enabled": false
                    },
                    "store": false,
                    "fielddata": {
                        "format": "disabled"
                    }
                },
                "isarticle": {
                    "type": "boolean",
                    "index": "not_analyzed",
                    "store": false,
                    "fielddata": {
                        "format": "disabled"
                    }
                },
                "ismobile": {
                    "type": "boolean",
                    "index": "not_analyzed",
                    "store": false,
                    "fielddata": {
                        "format": "disabled"
                    }
                },
                "isquestion": {
                    "type": "boolean",
                    "index": "not_analyzed",
                    "store": false,
                    "fielddata": {
                        "format": "disabled"
                    }
                },
                "isforum": {
                    "type": "boolean",
                    "index": "not_analyzed",
                    "store": false,
                    "fielddata": {
                        "format": "disabled"
                    }
                },
                "pubdate": {
                    "type": "date",
                    "index": "not_analyzed",
                    "store": false,
                    "null_value": "1970-01-01T00:00:00Z"
                },
                "sitelinks": {
                    "type": "string",
                    "index": "no",
                    "store": false,
                    "norms": {
                        "enabled": false
                    },
                    "fielddata": {
                        "format": "disabled"
                    }
                },
                "image": {
                    "type": "binary",
                    "index": "no",
                    "store": false,
                    "norms": {
                        "enabled": false
                    },
                    "fielddata": {
                        "format": "disabled"
                    }
                },
                "props": {
                    "type": "string",
                    "index": "no",
                    "store": false,
                    "norms": {
                        "enabled": false
                    },
                    "fielddata": {
                        "format": "disabled"
                    }
                },
                "scopes": {
                    "type": "string",
                    "index": "no",
                    "store": false,
                    "norms": {
                        "enabled": false
                    },
                    "fielddata": {
                        "format": "disabled"
                    }
                }
            }
        }
    }
}
