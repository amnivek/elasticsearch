{
    "settings": {
        "number_of_shards": 4,
        "number_of_replicas": 1,
        "index.refresh_interval": "300s"
    },
    "mappings": {
        "_default_": {
            "_all": {
                "enabled": false
            }
        },
        "cn": {
            "properties": {
                "pubdate": {
                    "type": "date",
                    "index": "not_analyzed",
                    "store": false,
                    "null_value": "1970-01-01T00:00:00Z"
                },
                "site": {
                    "type": "string",
                    "index": "not_analyzed",
                    "term_vector": "no",
                    "norms": {
                        "enabled": false
                    },
                    "fielddata": {
                        "format": "disabled"
                    }
                },
                "group": {
                    "type": "binary",
                    "index": "not_analyzed",
                    "store": true,
                    "fielddata": {
                        "format": "doc_values"
                    }
                },
                "appname": {
                    "type": "string",
                    "index": "analyzed",
                    "store": false,
                    "fielddata": {
                        "format": "disabled"
                    },
                    "term_vector": "with_positions_offsets",
                    "analyzer": "ik_index",
                    "search_analyzer": "ik_search"
                },
                "appname_cn": {
                    "type": "string",
                    "index": "no",
                    "store": false,
                    "fielddata": {
                        "format": "disabled"
                    }
                },
                "content": {
                    "type": "string",
                    "index": "analyzed",
                    "store": false,
                    "fielddata": {
                        "format": "disabled"
                    },
                    "term_vector": "with_positions_offsets",
                    "analyzer": "ik_index",
                    "search_analyzer": "ik_search"
                }
            }
        }
    }
}
